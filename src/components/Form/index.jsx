import React from "react"
import { Button, TextField } from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles"
import * as yup from 'yup'
import { useForm } from "react-hook-form"
import { yupResolver } from '@hookform/resolvers/yup'
import { useHistory } from "react-router-dom"

const useStyle = makeStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: '#fff',
        border: '2px solid #3f51b5',
        borderRadius: 5,
        padding: 10,
    },
    input: {
        margin: 10,
    }
})

const Form = () => {

    const classes = useStyle()
    const history = useHistory()

    const formSchema = yup.object().shape({
        name: yup.string().required("Nome Obrigatório"),
        email: yup.string().required("Email Obrigátorio").email("Email inválido"),
        password: yup.string().required("Senha Obrigatória").matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[$*&@#])[0-9a-zA-Z$*&@#]{8,}$/, "Mínimo 8 Dígitos"),
        comfirmPassword: yup.string().required("Confirme sua Senha").oneOf([yup.ref("password")], "Senha não confere!")
    })

    const { register, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(formSchema)
    })

    const onSubmitFunction = (data) => history.push(`/connected/${data.name}`)

    return (
        <div>
            <form onSubmit={handleSubmit(onSubmitFunction)} className={classes.root}>
                <TextField
                    className={classes.input}
                    label="Nome"
                    error={!!errors.name}
                    helperText={errors.name?.message}
                    required
                    {...register("name")}
                />
                <TextField
                    className={classes.input}
                    label="Email"
                    error={!!errors.email}
                    helperText={errors.email?.message}
                    required
                    {...register("email")}
                />
                <TextField
                    className={classes.input}
                    label="Senha"
                    type="password"
                    error={!!errors.password}
                    helperText={errors.password?.message}
                    required
                    {...register("password")}
                />
                <TextField
                    className={classes.input}
                    label="Confirmar Senha"
                    type="password"
                    error={!!errors.comfirmPassword}
                    helperText={errors.comfirmPassword?.message}
                    required
                    {...register("comfirmPassword")}
                />
                <Button
                    className={classes.input}
                    variant="contained"
                    color="primary"
                    type="submit"
                >
                    CADASTRAR
                </Button>
            </form>
        </div>
    )
}

export default Form