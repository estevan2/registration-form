import './App.css';
import { Switch, Route, useHistory } from "react-router-dom"
import Connected from './pages/connected';
import Form from './components/Form'

function App() {

  const history = useHistory()

  return (
    <div className="App">
      <div className="content">
        <Switch>
          <Route exact path="/">
            <Form />
          </Route>
          <Route exact path="/connected/:name">
            <Connected />
          </Route>
        </Switch>
      </div>
    </div>
  );
}

export default App;
