import Welcome from "./celebration.svg"
import React from "react";
import { useHistory, useParams } from "react-router-dom"
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";

const useStyles = makeStyles({
    root: {
        color: "#3f51b5"
    },
    image: {
        height: "300px"
    }
})

const Connected = () => {

    const params = useParams()
    const history = useHistory()
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <h1>Ben-vindo(a) {params.name}</h1>
            <img src={Welcome} alt="welcome" className={classes.image} />
            <Button onClick={() => history.push("/")} variant="contained" color="primary">Voltar</Button>
        </div>
    )
}

export default Connected